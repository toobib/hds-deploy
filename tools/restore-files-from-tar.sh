#!/bin/bash

set -e -u -o pipefail -o functrace -o errtrace -o nounset -o errexit

site=""
dry_run=""
do_cleanup="0"
action="restore"

# Parse arguments
while [[ $# -gt 0 ]]; do
  case "$1" in
    --site)
      site="$2"
      shift 2
      ;;
    --clean)
      action="clean"
      shift 1
      ;;
    --fix-permissions)
      action="fix-permissions"
      shift 1
      ;;
    --dry-run)
      dry_run="1"
      shift 1
      ;;
    --clean-up)
      do_cleanup="1"
      shift 1
      ;;
    *)
      echo "Unknown argument: $1"
      exit 1
      ;;
  esac
done

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
tmp_path="$(dirname "$SCRIPTPATH")/tmp"
tar_prefix="$tmp_path/restore_from_zip-"
sites_path="/home/frappe/frappe-bench/sites"

if [[ -n "$dry_run" ]]; then
  echo "Dry-run mode enabled. No changes will be made."
fi

if [[ "$action" == "clean" ]]; then
  params=("-print" "-delete")
  if [[ -n "$dry_run" ]]; then
    params=("-print")
  fi
  if [[ -z "$site" ]]; then
    echo "step: Cleaning up stale in-container TAR file copies for all sites"
    docker run --rm -it -v "production-bench_sites:$sites_path" busybox find "$sites_path" -maxdepth 2 -mindepth 2 -name '*.tar' "${params[@]}"
  else
    echo "step: Cleaning up stale in-container TAR file copies for site $site"
    docker run --rm -it -v "production-bench_sites:$sites_path" busybox find "$sites_path/$site" -maxdepth 1 -mindepth 1 -name '*.tar' "${params[@]}"
  fi
  echo "all done"
  exit 0
fi

if [[ "$action" == "fix-permissions" ]]; then
  if [[ -n "$dry_run" ]]; then
    docker run --rm -v "production-bench_sites:$sites_path" busybox ls -la "$sites_path/$site/public/files" "$sites_path/$site/private/files"
    exit 0
  fi
  docker run --rm -v "production-bench_sites:$sites_path" busybox chown -R "1000:1000" "$sites_path/$site/public/files" "$sites_path/$site/private/files"
  echo "done"
  exit 0
fi

if [[ -z "$site" ]]; then
  echo "ERROR: Missing --site argument"
  exit 1
fi

get_tar_file () {
  local found;
  found="$(find "$tar_prefix$1" -name "*_fr-$2.tar" | head -n 1)"
  if [[ -z "$found" ]]; then
    echo "ERROR: Missing file like $tar_prefix$1/*$2.tar for site $1" >&2
    exit 1
  fi

  # Check that the file is not empty
  if [[ ! -s "$found" ]]; then
    echo "ERROR: File $found is empty" >&2
    exit 1
  fi

  # Check that the file is a valid tar file
  if ! tar -tf "$found" &>/dev/null; then
    echo "ERROR: File $found is not a valid tar file" >&2
    exit 1
  fi
  echo "$found"
}

echo "step: Listing in-host TAR file from downloaded zip file"
find "$tar_prefix$site" -maxdepth 1 -mindepth 1 -name '*.tar' -print
echo

echo "step: Listing stale in-container TAR file copies"
docker run --rm -it -v "production-bench_sites:$sites_path" busybox find "$sites_path/$site" -maxdepth 1 -mindepth 1 -name '*.tar' -print
echo

# D'abord, on vérifie que les fichiers source existent pour mener à bien la restauration.
echo "step: Checking that source TAR files exist"
echo "- $site"
for kind in "files" "private-files"; do
  echo "  + $kind"
  source="$(get_tar_file "$site" "$kind")"
  echo "    * $source"
done
echo "done"
echo

# Ensuite, on vient supprimer les vieux fichiers TAR qui n'ont pas été supprimés à cause de l'échec de la restauration.
if [[ "$do_cleanup" == "1" ]]; then
  echo "step: Cleaning up stale in-container TAR file copies"
  if [[ -z "$dry_run" ]]; then
    docker run --rm -it -v "production-bench_sites:$sites_path" busybox find "$sites_path/$site" -maxdepth 1 -mindepth 1 -name '*.tar' -print -delete
  else
    echo "[dry-run] Would delete:"
    docker run --rm -it -v "production-bench_sites:$sites_path" busybox find "$sites_path/$site" -maxdepth 1 -mindepth 1 -name '*.tar' -print
  fi
  echo "done"
  echo
fi

# Enfin, on procède à la restauration
echo "step: Restoring files"
for kind in "files" "private-files"; do
  source="$(get_tar_file "$site" "$kind")"
  echo "* $source"
  if [[ -z "$dry_run" ]]; then
    docker run --rm -v "production-bench_sites:$sites_path" -v "$source:/mnt/source.tar:ro" busybox tar -xv -f /mnt/source.tar -C "$sites_path/"
  else
    echo "[dry-run] Would restore:"
    docker run --rm -v "production-bench_sites:$sites_path" -v "$source:/mnt/source.tar:ro" busybox tar -tv -f /mnt/source.tar
  fi
  echo
done

echo "step: Fixing permissions"
docker run --rm -v "production-bench_sites:$sites_path" busybox chown -R "1000:1000" "$sites_path/$site/public/files" "$sites_path/$site/private/files"

echo -n "Remaining disk space:"
df -h --total --output=avail | tail -n 1
echo

echo "all done"