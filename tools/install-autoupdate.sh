#!/bin/bash

# This script installs a systemd timer and service that runs every Sunday+Monday at 3:00 AM,
# and updates the deployed containers if there are any updates available.

service_name="maia-autoupdate"
command="/bin/sh -c 'cd /home/maia/hds-deploy && test -x manage.py && manage.py upgrade-manage && manage.py upgrade'"
service_file="/etc/systemd/system/${service_name}.service"
timer_file="/etc/systemd/system/${service_name}.timer"

# Create the service file
cat <<EOF > "${service_file}"
# Maia autoupdate by Dokos/InterHop/Toobib

[Unit]
Description=Automatic updates for Maia
Wants=${service_name}.timer

[Service]
Type=oneshot
ExecStart=${command}
User=maia

[Install]
WantedBy=multi-user.target
EOF

# Create the timer file
cat <<EOF > "${timer_file}"
# Maia autoupdate by Dokos/InterHop/Toobib
[Unit]
Description=Automatic updates for Maia
Requires=${service_name}.service

[Timer]
Unit=${service_name}.service
OnCalendar=Sun,Mon *-*-* 03:00:00

[Install]
WantedBy=timers.target
EOF

# Enable the timer
systemctl enable "${service_name}.timer"

# Start the timer
systemctl start "${service_name}.timer"

# Check the status of the timer
systemctl status "${service_name}.timer"
