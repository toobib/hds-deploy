# Restaurer les fichiers (Pièces jointes) depuis un fichier TAR

Lors de la restauration d'un site, le framework Frappe/Dodock restaure les fichiers de pièces jointes depuis un fichier TAR.
Or, pendant cette opération, le fichier TAR est d'abord copié à l'intérieur du conteneur Docker, puis extrait.
Dans des condition de faible espace disque, il peut arriver que l'extraction échoue, car le fichier TAR est trop volumineux pour être copié.
Ainsi, la restauration est partielle, et les fichiers ne sont donc pas restaurés.

Lorsque cette situation se produit, vous pouvez tout de même récupérer les fichiers depuis le fichier TAR en utilisant les commandes précisées ci-dessous.


## Faire un état des lieux

### Trouver les fichiers TAR locaux qui n'ont pas été supprimés à la fin de la restauration

La commande `manage.py restore-zip` télécharge les fichiers à l'intérieur du dossier tmp/, puis les supprime à la fin de la restauration sauf si elle échoue.

```sh
find ~/hds-deploy/tmp -maxdepth 2 -mindepth 2 -name '*.tar' -print 2>&1
```

### Trouver les fichiers TAR partiellement copiés dans le conteneur Docker
`production-bench_sites` est le nom du volume Docker contenant les sites Dodock, leur configuration, etc.

```sh
docker run --rm -it -v 'production-bench_sites:/home/frappe/frappe-bench/sites' busybox find /home/frappe/frappe-bench/sites -maxdepth 2 -mindepth 2 -name '*.tar' -print 2>&1
```

## Libérer de l'espace disque en supprimant les copies

### Supprimer les copies redondantes des fichiers TAR dans le conteneur Docker

```sh
docker run --rm -it -v 'production-bench_sites:/home/frappe/frappe-bench/sites' busybox find /home/frappe/frappe-bench/sites -maxdepth 2 -mindepth 2 -name '*.tar' -print -delete
```

### Restaurer un fichier TAR depuis tmp/ vers le conteneur Docker

```sh
docker run --rm -v "production-bench_sites:$sites_path" -v "$source:/mnt/source.tar:ro" busybox tar -xvf /mnt/source.tar -k -C "$sites_path/"
```

Si la commande tombe en erreur, c'est qu'un des fichier à restaurer existe déjà. Retirez alors l'option -k de la commande.
