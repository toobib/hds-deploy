# Déploiement sur un hébergement de données de santé

Ce projet a pour but de détailler les étapes de déploiement d'une application Dodock dockerisée sur un hébergement de données de santé.
L'objectif est de proposer un ensemble de commandes à exécuter pour déployer, mettre à jour et sauvegarder les instances (sites) de l'application.


## Prérequis

Voir le fichier [INSTALL.md](INSTALL.md)


## Configuration

```sh
git clone https://framagit.org/toobib/hds-deploy.git
./hds-deploy/manage.py setup  # Configuration interactive
./hds-deploy/manage.py up  # Démarrage
```

## Mise à jour

```sh
./hds-deploy/manage.py upgrade-manage
./hds-deploy/manage.py upgrade
```
