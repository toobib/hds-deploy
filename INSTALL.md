# Installation

## Avec root

### Mettre à jour les paquets
```sh
sudo apt-get -qq update -y
sudo apt-get -qq upgrade -y
```

### Installer Python 3.10


### Installer pip et requests
```sh
sudo apt-get -qq install -y python3-pip
python3 -m pip install --quiet requests
```


### Installer Docker
```sh
curl -fsSL https://get.docker.com | sudo bash
```


# Définir d'autres plages d'adresses IP pour les réseaux Docker si nécessaire
# echo '{"default-address-pools":[{"base": "192.168.0.0/16", "size": 24}]}' | sudo tee /etc/docker/daemon.json
# sudo systemctl restart docker


### Créer le compte utilisateur
```sh
sudo adduser --disabled-password --gecos "" toobib
sudo usermod -aG docker toobib
```


## En tant qu'utilisateur non-priviliégié

### Installer hds-deploy
```sh
sudo su - toobib

git clone https://framagit.org/toobib/hds-deploy.git hds-deploy
hds-deploy/manage.py install
```


### Configurer les sites
```sh
sudo su - toobib

ADMIN_PASSWORD="admin"
TRAEFIK_PASSWORD="traefik"  # accès dashboard Traefik (lecture seule)
DB_PASSWORD="root"  # mot de passe interne

hds-deploy/manage.py setup \
  --admin-password "$ADMIN_PASSWORD" \
  --db-password "$DB_PASSWORD" \
  --traefik-password "$TRAEFIK_PASSWORD" \
  --certificate "proxy" \
  --traefik-domain traefik-dashboard.domaine.fr \
  --docker-image "registry.gitlab.com/dokos/maia" \
  --default-apps "frappe,maia" \
  --sites instance.domaine.fr

hds-deploy/manage.py up
```

### Accéder au site sur https://instance.domaine.fr
