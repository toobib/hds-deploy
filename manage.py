#!/usr/bin/env python3

import argparse
import datetime
import json
import logging
import os
import platform
import re
import shutil
import subprocess
import sys
import tempfile
import textwrap
import time

try:
	import requests
except ImportError:
	subprocess.check_call([sys.executable, "-m", "pip", "install", "--quiet", "requests"])
	import requests

FRAPPE_DOCKER_SOURCE_URL = "https://github.com/frappe/frappe_docker.git"

os.chdir(os.path.dirname(os.path.abspath(__file__)))
HDS_DEPLOY_DIR = os.getcwd()

class CommandFailed(Exception):
	def __init__(self, message=None):
		self.message = message or "Command failed"


def hash_password(password: str):
	# openssl passwd -apr1 "$password" | sed 's/\$/\$\$/g'
	return subprocess.check_output(
		["openssl", "passwd", "-apr1", password], text=True
	).strip().replace("$", "$$")


def run_command(command, input: str = None, stdout=None, stdin=None, stderr=None, exit_on_error=True, capture=True, quiet=False):
	if isinstance(command, str):
		command = command.strip().split(" ")
	if input is not None:
		input = input.encode("utf8", "replace")

	try:
		if capture:
			stdout = subprocess.PIPE
		if input:
			stdin = subprocess.PIPE

		process = subprocess.Popen(
			command,
			stdin=stdin or sys.stdin,
			stdout=stdout or sys.stdout,
			stderr=stderr or sys.stderr,
		)

		out, error = "", ""
		if process.stdout:
			if input:
				process.stdin.write(input)
				process.stdin.close()

			iterator1 = iter(process.stdout.readline, b"")
			while iterator1:
				if iterator1:
					try:
						line = next(iterator1)
						if not quiet:
							cprint(line.decode("utf8", "replace"), dim=1, end="")
						out += line.decode("utf8", "replace")
					except StopIteration:
						iterator1 = None
			process.wait()
		else:
			out, error = process.communicate(input=input)
	except KeyboardInterrupt:
		out, error = None, None
		process.kill()

	if process.returncode != 0 and exit_on_error:
		msg = ""
		msg += f"An unexpected error ({process.returncode}) happened while runnning the following command:\n"
		msg += "    " + " ".join(command) + "\n" + "\n"

		if out is not None:
			msg += out + "\n"

		if error is not None:
			msg += error + "\n"

		raise CommandFailed(msg)

	return process.returncode, out, error


def generate_pass(length: int = 12) -> str:
	"""Generate random password using best available randomness source."""
	import math
	import secrets

	if not length:
		length = 56

	return secrets.token_urlsafe(math.ceil(length / 2))[:length]


def parse_site_list(sites: str) -> list:
	if not sites:
		return []
	"""Parse a comma separated list of backtick quoted sites."""
	return [site.strip("`") for site in sites.split(",")]


def format_site_list(sites: list) -> str:
	if not sites:
		return ""
	"""Format a list of sites as a comma separated list of backtick quoted sites."""
	return ",".join([f"`{site}`" for site in sites]).strip(",")


def parse_app_list(apps: str) -> list:
	if not apps:
		return []
	"""Parse a comma separated list of apps."""
	return [app.strip() for app in apps.split(",")]


class Manager:
	def Dotenv(self, name, template):
		return Dotenv(
			path=os.path.join(self.dir, name + ".env"),
			template=textwrap.dedent(template).strip()
		)

	def __init__(self, project) -> None:
		if not project:
			raise ValueError("Project name is required")

		root = os.path.dirname(HDS_DEPLOY_DIR)
		self.dir = os.path.join(root, project) + ".deploy"
		if not os.path.exists(self.dir):
			os.makedirs(self.dir)

		self.project = project

		self.environ = self.Dotenv("project", """
		# This file is auto-generated. Do not edit manually.

		DOCKER_IMAGE=

		# Administrator password
		ADMINISTRATOR_PASSWORD=

		# List of sites quoted with backtick and separated by comma
		SITES=

		## Let's Encrypt
		EMAIL=
		CERT_TYPE=
		WILDCARD_DOMAIN=

		## Traefik dashboard
		TRAEFIK_DOMAIN=
		HASHED_PASSWORD=

		# Database
		DB_PASSWORD=
		DB_HOST=mariadb-database
		DB_PORT=3306

		# Network
		ROUTER=
		BENCH_NETWORK=
		""")

		self.app_list = self.Dotenv("apps", """
		# List of apps to install for each site
		# lorem.ipsum.com=frappe,erpnext
		default=frappe
		""")

		self.existing_sites = list(self.all_sites)

	def is_running(self):
		code, out, err = self.run_backend_command("ps -q --services --status=running", quiet=True)
		if code != 0:
			return False
		if not isinstance(out, str):
			return False
		lines = out.strip().splitlines()
		if len(lines) > 0:
			return True
		return False

	def ensure_up(self):
		if not self.is_running():
			self.cmd_up()

	def needs_setup(self):
		return not (
			self.environ.get("EMAIL")
			and self.environ.get("TRAEFIK_DOMAIN")
			and self.environ.get("HASHED_PASSWORD")
			and self.environ.get("DB_PASSWORD")
			and self.environ.get("ROUTER")
			and self.environ.get("BENCH_NETWORK")
			and self.environ.get("DOCKER_IMAGE")
			and self.environ.get("ADMINISTRATOR_PASSWORD")
			and self.environ.get("WILDCARD_DOMAIN")
			and self.environ.get("CERT_TYPE")
			and self.app_list.get("default")
		)

	def cmd_setup(self, args):
		if not self.needs_setup():
			cprint("Setup already done", level=3, dim=1)

		boldprint("Setting up environment")
		self.environ.set_or_prompt("DB_PASSWORD", args.db_password, "Database password: ")
		self.environ.update(
			{
				"REDIS_CACHE": "redis-cache:6379",
				"REDIS_QUEUE": "redis-queue:6379",
				"REDIS_SOCKETIO": "redis-socketio:6379",
				"SOCKETIO_PORT": "9000",
			}
		)

		self.environ.set_or_prompt("CERT_TYPE", args.certificate, "HTTPS certificate (yes, no, proxy): ")
		if self.environ.get("CERT_TYPE") == "yes":
			self.environ.set_or_prompt("EMAIL", args.letsencrypt_email, "Let's Encrypt email address: ")
			self.environ.set_or_prompt("WILDCARD_DOMAIN", args.wildcard_domain, "Wildcard domain (type `-' to disable): ")
		else:
			self.environ.update({
				"EMAIL": "-",
				"WILDCARD_DOMAIN": "-",
			})

		wildcard_domain = self.environ.get("WILDCARD_DOMAIN")
		traefik_domain = args.traefik_domain
		if not traefik_domain and wildcard_domain and wildcard_domain != "-":
			traefik_domain = f"traefik.{wildcard_domain}"
		self.environ.set_or_prompt("TRAEFIK_DOMAIN", traefik_domain, "Traefik dashboard domain: ")

		hashed_password = self.environ.get("HASHED_PASSWORD")
		if not hashed_password or args.traefik_password:
			traefik_password = (
				args.traefik_password
				or input("Traefik dashboard password: ").strip()
			)
			hashed_password = hash_password(traefik_password)
			self.environ.update({
				"HASHED_PASSWORD": hash_password(traefik_password),
			})

		self.environ.set_or_prompt("DOCKER_IMAGE", args.docker_image, "Docker image: ")

		self.environ.update({
			"ROUTER": self.project,
			"BENCH_NETWORK": self.project,
			"SITES": format_site_list(self.all_sites),
		})

		prev_password = self.environ.get("ADMINISTRATOR_PASSWORD")
		if not prev_password:
			self.environ.set_or_prompt("ADMINISTRATOR_PASSWORD", args.admin_password, "Administrator password: ")
		elif args.admin_password and prev_password != args.admin_password:
			raise RuntimeError("Administrator password already set, use `set-admin-password` to change it")

		self.app_list.set_or_prompt("default", args.default_apps, "Default apps (comma separated without whitespace): ")

		if args.sites:
			self.cmd_up()
			self.cmd_add_sites(str(args.sites).split(","))

	def cmd_pull(self, args=None):
		boldprint("Pulling images")
		self.run_traefik_command("pull")
		self.run_database_command("pull")
		self.run_backend_command("pull")

	def cmd_docker(self, docker_command):
		self.run_backend_command(docker_command, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr, capture=False)

	def cmd_bench(self, bench_args):
		if not self.is_running():
			cprint("Error: Project is not running.", level=1, bold=1)
			sys.exit(1)
		self.run_backend_command("exec", "--no-TTY", "backend", "bench", bench_args)

	def cmd_shell(self):
		if not self.is_running():
			cprint("Warning: Project is not running.", level=3, bold=1)
			cprint()
		self.run_backend_command("run", "--rm", "-it", "--no-deps", "backend", "bash", stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr, capture=False)

	def cmd_backup(self):
		now_dt = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		backup_dir = os.path.join(self.dir, "backups")
		backup_dir = os.path.join(backup_dir, now_dt)
		os.makedirs(backup_dir, exist_ok=True)

		self.run_backend_command([
			"run",
			"--rm",
			"--name",
			"--volume",
			f"{self.project}-backup-{now_dt}",
			f"{backup_dir}:/backups",
			"backend",
			"backup",
		])

	def cmd_up(self, *args: str):
		boldprint("Starting services")
		args = args or ["-d"]
		self.run_traefik_command("up", *args)
		self.run_database_command("up", *args)
		self.run_backend_command("up", *args)

	def cmd_down(self):
		boldprint("Stopping services")
		self.run_backend_command("down")
		self.run_database_command("down")
		self.run_traefik_command("down")

	def cmd_status(self):
		boldprint("Status")
		self.run_traefik_command("ps")
		print()
		self.run_database_command("ps")
		print()
		self.run_backend_command("ps")

	def cmd_restart(self):
		boldprint("Restarting services")
		self.cmd_down()
		self.cmd_up()
		# self.run_database_command("restart")
		# self.run_backend_command("restart", "frontend")
		# self.run_traefik_command("restart")

	def cmd_destroy(self):
		boldprint("Destroying services")
		self.run_backend_command("down", "--volumes", "--remove-orphans")
		self.run_database_command("down", "--volumes", "--remove-orphans")
		self.run_traefik_command("down", "--volumes", "--remove-orphans")

	def cmd_upgrade(self, to_tag: str | None = None, to_image: str | None = None, force=False):
		old_docker_image = self.environ.get("DOCKER_IMAGE")
		new_docker_image = self.environ.get("DOCKER_IMAGE")

		# Replace tag if specified
		if to_image:
			new_docker_image = to_image
		if to_tag:
			new_docker_image = new_docker_image.split(":")[0] + ":" + to_tag

		boldprint(f"Upgrading {old_docker_image!r} to {new_docker_image!r}")

		def get_image_hash(image: str):
			cmd = ["docker", "inspect", "--format={{index .RepoDigests 0}}", image]
			img_hash = run_command(cmd)[1]
			return img_hash.strip()

		# Pull latest images
		old_image_hash = get_image_hash(old_docker_image)
		self.cmd(["docker", "pull", new_docker_image])
		new_image_hash = get_image_hash(new_docker_image)
		cprint()

		# Check if image hash changed
		if old_image_hash == new_image_hash:
			cprint(f"Image for {new_docker_image} is already up to date.", level=2)
			if not force:
				return

		cprint("Backing up current config (pinning docker image hash).")

		# Make copy of current config project.env
		with open(self.dir + "/project.env", "r") as f:
			current_config_data = f.read()

		# Remove old backup
		if os.path.exists(self.dir + "/project.bak.env"):
			os.remove(self.dir + "/project.bak.env")
		bak_env = self.Dotenv("project.bak", current_config_data)
		bak_env.update({ "DOCKER_IMAGE": old_image_hash })
		cprint("Saved backup to:", bak_env.path)
		cprint()

		# Update project.env
		self.environ.update({ "DOCKER_IMAGE": new_docker_image })

		cprint("Recreating services...")
		self.cmd_down()  # Always recreate services when upgrading to avoid issues with stale assets (e.g. JS/CSS)
		self.cmd_up()
		cprint()

		# Run upgrade
		if len(self.all_sites):
			cprint("Migrating sites...")

			try:
				self.cmd_bench(["set-config", "-gp", "allow_reads_during_maintenance", "1"])

				for site in self.all_sites:
					try:
						self.cmd_bench(["--site", site, "set-maintenance-mode", "on"])
					except Exception as e:
						cprint(f"*** Error while enabling maintenance mode for site {site}", level=1)
						logging.exception(f"*** Error while enabling maintenance mode for site {site}")

				for site in self.all_sites:
					try:
						self.cmd_bench(["--site", site, "migrate", "--skip-failing", "--skip-search-index"])
					except Exception as e:
						cprint(f"*** Error while migrating site {site}", level=1)
						cprint(e)
						logging.exception(f"*** Error while migrating site {site}")

					try:
						self.cmd_bench(["--site", site, "set-maintenance-mode", "off"])
					except Exception as e:
						cprint(f"*** Error while disabling maintenance mode for site {site}", level=1)
						logging.exception(f"*** Error while disabling maintenance mode for site {site}")

			except Exception as e:
				cprint("*** Error while migrating sites.")
				cprint(e)
				logging.exception("*** Error while migrating sites")

				for site in self.all_sites:
					try:
						self.cmd_bench(["--site", site, "set-maintenance-mode", "off"])
					except Exception as e:
						cprint(f"*** Error while disabling maintenance mode for site {site}")
						logging.exception(f"*** Error while disabling maintenance mode for site {site}")

				# logging.error("Rolling back to previous version")
				# self.environ.update({ "DOCKER_IMAGE": old_image_hash })
				# self.cmd_down()
				# self.cmd_up()

		else:
			cprint("No site to migrate after upgrade.")

		cprint("Done.")
		return True

	def run_traefik_command(self, *cmd: str, **kwargs):
		extensions = []

		if self.environ.get("CERT_TYPE") == "yes":
			extensions += ["-f", "{frappe_docker}/overrides/compose.traefik-ssl.yaml"]
		if self.environ.get("CERT_TYPE") == "proxy":
			extensions += ["-f", "./compose/compose.traefik-sslnocert.yaml"]

		return self.cmd("""
		docker compose
			--project-name {project_name}-traefik
			--env-file {config_dir}/project.env
			-f {frappe_docker}/overrides/compose.traefik.yaml
		""", *extensions, *cmd, **kwargs)

	def run_database_command(self, *cmd: str, **kwargs):
		return self.cmd("""
		docker compose
			--project-name {project_name}-database
			--env-file {config_dir}/project.env
			-f {frappe_docker}/overrides/compose.mariadb-shared.yaml
		""", *cmd, **kwargs)

	def run_backend_command(self, *cmd: "str | list[str]", **kwargs):
		extensions = []

		if self.environ.get("WILDCARD_DOMAIN", "-") != "-":
			extensions += ["-f", "./compose/compose.wildcard.yaml"]
		if self.environ.get("CERT_TYPE") == "yes":
			extensions += ["-f", "{frappe_docker}/overrides/compose.multi-bench-ssl.yaml"]
		if self.environ.get("CERT_TYPE") == "proxy":
			extensions += ["-f", "./compose/compose.multi-bench-sslnocert.yaml"]

		extensions += ["-f", "./compose/compose.restart-on-boot.yaml"]

		return self.cmd("""
		docker compose
			--project-name {project_name}-bench
			--env-file {config_dir}/project.env
			-f ./compose/compose.yaml
			-f {frappe_docker}/overrides/compose.redis.yaml
			-f {frappe_docker}/overrides/compose.multi-bench.yaml
		""", *extensions, *cmd, **kwargs)

	@property
	def frappe_docker_path(self):
		return os.path.abspath("frappe_docker")

	def cmd(self, *parts: list | str, params: dict = {}, **kwargs):
		quiet = kwargs.pop("quiet", False)
		params = {
			"{project_name}": self.project,
			"{config_dir}": self.dir,
			"{frappe_docker}": self.frappe_docker_path,
			**params
		}

		command = []
		for part in parts:
			if isinstance(part, list):
				command.extend(part)
			elif isinstance(part, str):
				command.extend(part.strip().split())
			else:
				command.append(str(part))

		for key, value in params.items():
			command = [part.replace(key, value) for part in command]

		if not quiet:
			cprint("+", self.redact("  ".join(command)), dim=1)
		return run_command(command, **kwargs, quiet=quiet)

	def redact(self, s: str):
		if ap := self.environ.get("ADMINISTRATOR_PASSWORD"):
			s = s.replace(ap, "***admin***")
		if dp := self.environ.get("DB_PASSWORD"):
			s = s.replace(dp, "***database***")
		if tp := self.environ.get("TRAEFIK_PASSWORD"):
			s = s.replace(tp, "***traefik***")
		return s

	@property
	def all_sites(self):
		return parse_site_list(self.environ.get("SITES"))

	def apps_for_site(self, site):
		return parse_app_list(self.app_list.get(site, "") or self.app_list.get("default"))

	def real_site_list(self):
		# for site in sites/*; do [ -x "sites/$site/site_config.json" ] && basename "$site"; done
		x, ls, z = self.run_backend_command("run", "--rm", "--no-deps", "backend", "bash", "-c", ['for site in sites/*; do [ -f "$site/site_config.json" ] && basename "$site"; done; exit 0'], capture=True, stdout=subprocess.PIPE)
		return [site.strip() for site in ls.splitlines() if site.strip()]

	def cmd_fix_site_list(self):
		from_dir = set(self.real_site_list())
		from_env = set(self.all_sites)

		cprint("Sites from sites/:", from_dir)
		cprint("Sites from project.env:", from_env)

		if from_dir - from_env:
			boldprint("Warning: The following sites have a directory in sites/ but are not registered in project.env:")
			for site in from_dir - from_env:
				print(f"  {site}")

			ans = input("What to do? [drop/register/Nothing] ")
			ans = ans.strip().lower()
			if ans == "drop":
				self.cmd_drop_sites(list(from_dir - from_env))
			elif ans == "register":
				self.cmd_add_sites(list(from_dir - from_env))
			else:
				print("Doing nothing")

		if from_env - from_dir:
			boldprint("Warning: The following sites are registered in project.env but do not actually exist in sites/:")
			for site in from_env - from_dir:
				print(f"  {site}")

			ans = input("What to do? [create/forget/Nothing] ")
			ans = ans.strip().lower()
			if ans == "create":
				self.cmd_add_sites(list(from_env - from_dir))
			elif ans == "forget":
				self.environ.update({ "SITES": format_site_list(list(from_dir)) })
			else:
				print("Doing nothing")

	def cmd_site_list(self):
		if not self.all_sites:
			boldprint("No sites configured, use 'manage.py site new <site>' to new a site")
			return
		boldprint("Sites:")
		for site in self.all_sites:
			print(f"  {site}")

	def cmd_site_new(self, site: str, apps: str | None):
		if site in self.all_sites:
			boldprint(f"warning: Site {site} already exists")

		if apps:
			apps = parse_app_list(apps)
		else:
			apps = self.apps_for_site("default")

		self.app_list.update({ site: ",".join(apps) })
		self.cmd_add_sites([site])

	def cmd_site_drop(self, site: str):
		if site not in self.all_sites:
			boldprint(f"warning: Site {site} does not exist")

		self.cmd_drop_sites([site])

	def cmd_site_rename(self, site: str, new_name: str):
		if site not in self.all_sites:
			boldprint(f"warning: Site {site} does not exist")

		existing_sites = self.all_sites
		sites_after_rename = set(existing_sites)
		sites_after_rename.remove(site)
		sites_after_rename.add(new_name)
		apps = self.app_list.get(site)

		self.run_backend_command(["run", "--rm", "--no-deps", "backend", "mv", f"sites/{site}", f"sites/{new_name}"])
		self.environ.update({
			"SITES": format_site_list(sorted(sites_after_rename)),
		})
		self.app_list.update({
			site: "",
			new_name: apps,
		})

		self.cmd_up()
		self.existing_sites = self.all_sites

	def cmd_site_console(self, site: str):
		self.run_backend_command("exec", "-it", "backend", "bench", "--site", site, "console", stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr, capture=False)

	def cmd_add_sites(self, sites: list):
		existing_sites = self.all_sites
		new_sites = set(existing_sites + sites)
		self.environ.update({
			"SITES": format_site_list(new_sites),
		})
		self.do_manage_sites(created=sites)

	def cmd_drop_sites(self, sites: list):
		existing_sites = self.all_sites
		new_sites = set(existing_sites) - set(sites)
		self.environ.update({
			"SITES": format_site_list(new_sites),
		})
		self.do_manage_sites(dropped=sites)

	def do_manage_sites(self, dropped=[], created=[]):
		existing_sites = self.existing_sites
		desired_sites = self.all_sites
		missing_sites = (set(desired_sites) - set(existing_sites)) | set(created)
		extra_sites = (set(existing_sites) - set(desired_sites)) | set(dropped)

		if missing_sites:
			boldprint("Creating sites:", *missing_sites)
			for site in missing_sites:
				try:
					self.do_create_site(site)
				except CommandFailed as e:
					if "already exists" in e.message:
						cprint("Site", site, "already exists", level=3)
					else:
						cprint("Failed to create site", site, "with error:", level=3)
						cprint(e, level=3, dim=1)

			cprint()

		if extra_sites:
			boldprint("Removing sites:", *extra_sites)
			for site in extra_sites:
				self.do_drop_site(site)

		self.cmd_up()
		self.existing_sites = self.all_sites

	def do_create_site(self, site):
		cprint("Creating site", site)
		password = self.environ.get("ADMINISTRATOR_PASSWORD")
		db_password = self.environ.get("DB_PASSWORD")

		cmd_1 = [
			"bench",
			"new-site",
			site,
			"--no-mariadb-socket",
			"--mariadb-root-password",
			db_password,
			"--admin-password",
			password,
			*(f"--install-app {app}" for app in self.apps_for_site(site))
		]
		self.run_backend_command(["exec", "--no-TTY", "backend", "bash", "-xc", " ".join(cmd_1)])

		cmd_2 = ["bench", "--site", site, "set-config", "hostname", f"https://{site}"]
		self.run_backend_command(["exec", "--no-TTY", "backend", "bash", "-xc", " ".join(cmd_2)])

	def do_drop_site(self, site):
		cprint("Removing site", site)

		db_password = self.environ.get("DB_PASSWORD")
		try:
			self.run_backend_command([
				"exec",
				"--no-TTY",
				"backend",
				"bench",
				"drop-site",
				site,
				"--force",
				"--mariadb-root-password",
				db_password
			])
		except CommandFailed:
			cprint("Site", site, "does not exist", level=3)
			raise
		cprint("Site", site, "dropped", level=2)

	def cmd_set_admin_password(self, password):
		self.run_backend_command([
			"exec",
			"--no-TTY",
			"backend",
			"bench",
			"--site",
			"all",
			"set-admin-password",
			password,
		])
		self.environ.update({ "ADMINISTRATOR_PASSWORD": password })

	def cmd_install_app(self, site, app):
		self.run_backend_command([
			"exec",
			"--no-TTY",
			"backend",
			"bench",
			"--site",
			site,
			"install-app",
			app,
		])

		new_list = ",".join(set(self.apps_for_site(site) + [app]))
		if site == "all":
			self.app_list.update({ s: new_list for s in self.all_sites })
		else:
			self.app_list.update({ site: new_list })

	def cmd_site_migrate(self, site):
		self.cmd_bench(["--site", site, "migrate", "--skip-failing", "--skip-search-index"])

	def cmd_site_backup(self, site):
		self.cmd_bench([
			"--site",
			site,
			"backup",
			"--with-files",
			"--compress",
		])

	def cmd_site_restore(self, site: str, zip_file = "local", zip_password = "", zip_cookies = "", exclude_apps = ""):
		if zip_file and zip_file != "local":
			return self.do_restore_from_zip(site, zip_file, zip_password, cookies=zip_cookies, exclude_apps=parse_app_list(exclude_apps))

		raise NotImplementedError("Restore from local backups not implemented yet")
		# cprint("Restoring from local in-Docker backups")
		# Run command to list backups
		# self.run_backend_command([
		# 	"exec",
		# 	"--no-TTY",
		# 	"backend",
		# 	"bench",
		# 	"--help",
		# ])
		# ./{site}/private/backups/20230426_153820-dokompany-site_config_backup.json
		# ./{site}/private/backups/20230426_153820-dokompany-database.sql.gz
		# ./{site}/private/backups/20230426_153820-dokompany-files.tgz
		# ./{site}/private/backups/20230426_153820-dokompany-private-files.tgz

	def do_restore_from_zip(self, site: str, zip_file: str, zip_password = "", cookies = "", exclude_apps = []):
		boldprint(f"Restoring from zip file {zip_file} for site {site}")
		os.makedirs("tmp", exist_ok=True)

		if site not in self.all_sites:
			cprint("Site", site, "does not exist", level=3)
			self.cmd_add_sites([site])

		self.cmd_bench(["--site", site, "set-maintenance-mode", "on"])

		# 1. Download zip file if it's a URL
		if zip_file and zip_file.startswith("http"):
			cprint("Downloading zip file from", zip_file)
			VALID_EXTENSIONS = ["zip", "sql.gz"]
			extension = next((e for e in VALID_EXTENSIONS if zip_file.endswith("." + e)), None)
			if not extension:
				raise ValueError("Invalid zip file extension")

			filename = os.path.basename(zip_file)
			zip_file_path = f"tmp/restore_from_zip-{site}-{filename}"

			if cookies:
				cookies = { c.split("=")[0].strip(): c.split("=")[1].strip() for c in cookies.split(";") }
			else:
				cookies = {}

			with requests.get(zip_file, cookies=cookies, stream=True) as r:
				with open(zip_file_path, "wb") as f:
					shutil.copyfileobj(r.raw, f)

			# r = requests.get(zip_file, cookies=cookies)
			# with open(zip_file_path, "wb") as f:
			# 	f.write(r.content)
		elif zip_file:
			zip_file_path = zip_file

		if not os.path.exists(zip_file_path):
			raise ValueError("Zip file not found")

		# 2. Extract zip file
		dest_dir = f"tmp/restore_from_zip-{site}"
		if os.path.exists(dest_dir):
			shutil.rmtree(dest_dir)
		os.makedirs(dest_dir)

		if zip_file_path.endswith(".zip"):
			from zipfile import ZipFile
			with ZipFile(zip_file_path, "r") as zip:
				cprint("Unzipping file.")
				zip.extractall(dest_dir, pwd=zip_password.encode("utf8"))
		elif "database.sql" in zip_file_path:
			filename = os.path.basename(zip_file)
			newpath = os.path.join(dest_dir, filename)
			shutil.move(zip_file_path, newpath)
			zip_file_path = newpath

		if zip_file and zip_file.startswith("http") and os.path.exists(zip_file_path):
			cprint("Deleting downloaded zip file.")
			os.remove(zip_file_path)

		self.restore_from_dir(site, dest_dir, exclude_apps)

		# Cleanup
		cprint("Cleaning up")
		shutil.rmtree(dest_dir)

	def restore_from_dir(self, site, source_dir, exclude_apps):
		backup_files = {
			"site_config": None,
			"database": None,
			"files": None,
			"private-files": None,
		}

		for backup_file_name in os.listdir(source_dir):
			for key in backup_files.keys():
				if f"{key}." in backup_file_name:
					if key == "files" and "private-files" in backup_file_name:
						continue
					backup_files[key] = os.path.join(source_dir, backup_file_name)

		for key, path in backup_files.items():
			if path:
				cprint(f"Found {key} backup file: {path}")

		if not backup_files["database"]:
			raise ValueError("Database backup file not found")

		if backup_files["database"].endswith(".gz"):
			# Check if really a gzip file
			with open(backup_files["database"], "rb") as f:
				if f.read(2) == b"\x1f\x8b":
					cmd = ["gzip", "-kd", backup_files["database"]]
					cprint("Decompressing database backup file")
				else:
					cmd = ["mv", backup_files["database"], backup_files["database"][:-3]]
					cprint("Renaming database backup file: it is not actually a gzip file")
			subprocess.run(cmd, check=True)
			backup_files["database"] = backup_files["database"][:-3]

		# 4. Parse site_config.json to extract encryption key
		encryption_key = None
		if backup_files["site_config"]:
			with open(backup_files["site_config"], "r") as f:
				try:
					encryption_key = json.load(f).get("encryption_key")
				except json.decoder.JSONDecodeError as e:
					print("Could not parse site_config.json file")
					print(e)

		container_tmp_root = "/tmp/restore"
		in_container_files = {
			key: os.path.join(container_tmp_root, os.path.basename(path))
			for key, path in backup_files.items()
			if path
		}

		base_cmd = [
			"run",
			"--rm",
			"--no-deps",
			"--volume",
			f"{os.path.abspath(source_dir)}:{container_tmp_root}",
			"backend",
		]
		restore_cmd = [
			*base_cmd,
			"bench",
			"--site",
			site,
			"restore",
			"--mariadb-root-password",
			self.environ.get("DB_PASSWORD"),
			"--admin-password",
			self.environ.get("ADMINISTRATOR_PASSWORD"),
			in_container_files.get("database"),
		]

		if encryption_key:
			restore_cmd.extend([
				"--encryption-key",
				encryption_key,
			])

		if path := in_container_files.get("files"):
			restore_cmd.extend(["--with-public-files", path])

		if path := in_container_files.get("private-files"):
			restore_cmd.extend(["--with-private-files", path])

		if in_container_files.get("site_config"):
			cprint("Restoring site config")
			keep_keys = [
				"db_name",
				"db_password",
				"db_type",
				# "encryption_key",
				"host_name",
				"maintenance_mode",
				"redis_cache",
				"redis_queue",
				"redis_socketio",
				"ssl_certificate_key",
				"ssl_certificate",
			]

			x0 = f"/home/frappe/frappe-bench/sites/{site}/site_config.json"
			x1 = in_container_files["site_config"]

			if " " in x1 or " " in x0:
				raise ValueError("Spaces in path not supported")

			cprint(f"Applying {x1} to {x0}")
			def mapper(key):
				return f"{key}:.[0].{key}"

			keep_keys_cmd = "{" + ",".join(map(mapper, keep_keys)) + "}"
			shell_cmd = [
				"jq", "-s",
				"'.[0] * (.[1] // {}) * " + keep_keys_cmd + " | del(..|nulls)'",
				x0, x1,
				f"> {x0}.new && mv {x0} {x0}.bak && mv {x0}.new {x0}"
			]
			shell_cmd = "\t".join(shell_cmd)

			self.run_backend_command([
				*base_cmd,
				"bash",
				"-c",
				shell_cmd,
			])

		cprint("Running restore command")
		self.run_backend_command(restore_cmd, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr, capture=False)

		for app in exclude_apps:
			cprint("Removing excluded app", app)
			self.cmd_bench(["--site", site, "remove-from-installed-apps", app])

		cprint("Migrating site")
		self.cmd_site_migrate(site)

		cprint("Migrating site (again)")
		self.cmd_site_migrate(site)

		cprint("Setting admin password")
		self.cmd_bench(["--site", site, "set-admin-password", self.environ.get("ADMINISTRATOR_PASSWORD")])
		self.cmd_bench(["--site", site, "set-maintenance-mode", "off"])

		cprint("Restored site successfully.", level=2, bold=1)

	def log_remote(self, *args, **kwargs):
		text = " ".join(map(repr, args))
		if kwargs:
			text += "\n" + json.dumps(kwargs, indent=2)

		text = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + " " + text
		text = text.replace(self.environ.get("ADMINISTRATOR_PASSWORD"), "$$ADMINISTRATOR_PASSWORD$$")
		text = text.replace(self.environ.get("DB_PASSWORD"), "$$DB_PASSWORD$$")
		cprint(text, dim=1)
		self.ping_remote(type="log", text=text)

	def ping_remote(self, **params):
		if url := self.environ.get("REMOTE_URL", "https://dashboard.dokos.io/api/method/maia_ping"):
			try:
				response = requests.post(url, json=params)
				if response.status_code != 200:
					cprint(f"Error pinging remote: {response.text}")
			except Exception as e:
				cprint(f"Error pinging remote: {e}")

	def cmd_setup_autoupdate(self):
		# Run shell file ./tools/install-autoupdate.sh
		path = os.path.join(HDS_DEPLOY_DIR, "tools/install-autoupdate.sh")
		if os.path.exists(path):
			cprint("Executing", path)
			subprocess.run(["bash", path], check=True)


logging.basicConfig(
	filename="log_manage.log",
	filemode="w",
	format="%(asctime)s %(levelname)s | %(message)s",
	level=logging.INFO,
)

def supports_color():
	return hasattr(sys.stdout, 'isatty') and sys.stdout.isatty()

USE_COLORS = supports_color()

def cprint(*args, level=0, dim=False, bold=False, reverse=False, end='\n'):
	styles = []

	if USE_COLORS:
		if level:
			styles.append('3' + str(level))
		if dim:
			styles.append('2')
		if bold:
			styles.append('1')
		if reverse:
			styles.append('7')

	if styles:
		print(f'\033[{";".join(styles)}m', end='')

	print(*args, end='')
	logging.info(" ".join(map(str, args)))

	if styles:
		print('\033[0m', end='')
	print(end=end)


def boldprint(*msg):
	cprint(*msg, level=0, bold=1)
	logging.info(" ".join(msg))


class Dependencies:
	def install_all(self):
		self.install_docker()
		self.clone_frappe_docker_repo()

	def install_docker(self):
		if shutil.which("docker") is not None:
			return cprint("Docker is already installed", level=2)

		cprint("Docker is not installed, Installing Docker...", level=3)
		logging.info("Docker not found, installing Docker")
		if platform.system() == "Darwin" or platform.system() == "Windows":
			cprint(
				f"""
				This script doesn't install Docker on {"Mac" if platform.system()=="Darwin" else "Windows"}.

				Please go through the Docker Installation docs for your system and run this script again"""
			)
			logging.debug("Docker setup failed due to platform is not Linux")
			sys.exit(1)
		try:
			ps = subprocess.run(
				["curl", "-fsSL", "https://get.docker.com"],
				capture_output=True,
				check=True,
			)
			# curl -fsSL https://get.docker.com | sudo bash
			# sudo usermod -aG docker $USER
			subprocess.run(["/bin/bash"], input=ps.stdout, capture_output=True)
			subprocess.run(
				["sudo", "usermod", "-aG", "docker", str(os.getenv("USER"))], check=True
			)
			cprint("Waiting for Docker to start... (10 seconds)", level=3)
			time.sleep(10)
			subprocess.run(["sudo", "systemctl", "restart", "docker.service"], check=True)
		except Exception as e:
			logging.error("Installing Docker failed", exc_info=True)
			cprint("Failed to Install Docker\n", e, level=1)
			cprint("\n Try Installing Docker Manually and re-run this script again\n", level=1)
			sys.exit(1)

	def clone_frappe_docker_repo(self):
		if os.path.exists("frappe_docker/"):
			shutil.rmtree("frappe_docker/")

		if FRAPPE_DOCKER_SOURCE_URL.endswith(".git"):
			try:
				cprint(f"Cloning frappe_docker from {FRAPPE_DOCKER_SOURCE_URL}...")
				subprocess.run(
					["git", "clone", FRAPPE_DOCKER_SOURCE_URL, "frappe_docker"],
					check=True,
				)
				logging.info("Cloned frappe_docker")
				return
			except Exception as e:
				logging.error("Cloning frappe_docker failed", exc_info=True)
				cprint("\nCloning frappe_docker Failed\n")
				return

		try:
			cprint(f"Downloading frappe_docker.zip from {FRAPPE_DOCKER_SOURCE_URL}...")
			r = requests.get(FRAPPE_DOCKER_SOURCE_URL, allow_redirects=True)
			with open("frappe_docker.zip", "wb") as f:
				f.write(r.content)

			shutil.unpack_archive("frappe_docker.zip", ".")
			# Unzipping the frappe_docker.zip creates a folder "frappe_docker-main"
			shutil.move("frappe_docker-main", "frappe_docker")
			logging.info("Unzipped and Renamed frappe_docker")
			os.remove("frappe_docker.zip")
			logging.info("Removed the downloaded zip file")
			return
		except Exception as e:
			logging.error("Download and unzip failed", exc_info=True)
			cprint("\nCloning frappe_docker Failed\n\n", "[ERROR]: ", e, level=1)
			return

	def get_current_hash(self):
		try:
			current_branch = subprocess.run(
				["git", "rev-parse", "HEAD"],
				capture_output=True,
				check=True,
			).stdout.decode("utf-8").strip()
			return current_branch
		except Exception as e:
			logging.error("Getting current branch failed", exc_info=True)
			cprint("\nGetting current branch failed\n", e, level=1)
			return

	def upgrade_self(self):
		# Check if the script is running from a git repo
		if not os.path.exists(".git"):
			cprint("Not a git repo, skipping upgrade", level=2)
			return

		# Pull the latest changes
		try:
			cprint("Pulling latest changes...")
			hash1 = self.get_current_hash()
			subprocess.run(["git", "pull", "--rebase"], check=True)
			hash2 = self.get_current_hash()
			logging.info("Pulled latest changes")
			if hash1 == hash2:
				cprint("Already up-to-date", hash2, level=2)
				return
		except Exception as e:
			logging.error("Pulling latest changes failed", exc_info=True)
			cprint("\nPulling latest changes failed\n", e, level=1)
			return

		cprint("Successfully upgraded this script and compose files, run `manage.py restart` to apply changes", level=2)
		return True

class Dotenv:
	def __init__(self, path, template=''):
		self.path = path
		self.template = template

		self._cache = None

	def update(self, vars: dict):
		try:
			# cprint(f"📝 Updating environment file {self.path}… ", end="")

			txt = self.template
			if os.path.exists(self.path) and os.path.isfile(self.path):
				with open(self.path, "r") as f:
					txt = f.read()

			file = open(self.path, "w")
			remaining_keys = set(vars.keys())

			for line in txt.splitlines():
				line = line.rstrip()
				splits = line.split("=", 1)
				if len(splits) == 2:
					key, old_value = splits
					key = key.strip()
					if (key in vars) and (vars[key] is not None):
						line = f"{key}={vars[key]}"
						remaining_keys.remove(key)

				print(line, file=file)

			if len(remaining_keys) > 0:
				print("", file=file)
				for key in remaining_keys:
					print(f"{key}={vars[key]}", file=file)

			file.close()
			self._cache = None
			# cprint("done.", level=2)
		except Exception as error:
			cprint(f"📝 Updating environment file {self.path} failed.", level=1)
			cprint()
			cprint(f"The following error happened while writing the environment file {self.path}:", level=1)
			raise error

	def read(self):
		if self._cache:
			return self._cache

		env_regex = re.compile(r"""^([^\s=]+)=(?:[\s"']*)(.*?)(?:[\s"']*)$""")
		result = {}

		if os.path.exists(self.path) and os.path.isfile(self.path):
			with open(self.path, 'r') as f:
				for line in f:
					match = env_regex.match(line)
					if match is not None:
						result[match.group(1)] = match.group(2)

		self._cache = result
		return result

	def get(self, key, default=None):
		return self.read().get(key, default)

	def set_or_prompt(self, key, value, prompt):
		"""Set a value in the environment file, or prompt if empty"""
		current_value = self.get(key)
		new_value = value
		if not current_value and not new_value:
			if not sys.stdin.isatty():
				raise RuntimeError(f"Terminal is not interactive, cannot prompt for value of {key}")
			new_value = input(prompt or f"{key}: ").strip()
		if not new_value:
			new_value = current_value
		if new_value != current_value:
			self.update({ key: new_value })


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Deployment manager for Frappe/Dodock")

	parser.add_argument(
		"--project",
		help="Environment name (e.g. production, preprod). NOTE: Database is shared between all environments",
		default="production",
	)

	# parser.add_argument(
	# 	"--site",
	# 	help="Site name",
	# 	action="append",
	# 	dest="sites",
	# 	default=[],
	# )

	commands = parser.add_subparsers(dest="command", help="Command to run")

	command_install = commands.add_parser("install", help="Install dependencies")

	command_setup = commands.add_parser("setup", help="Setup environment")
	command_setup.add_argument("--admin-password", help="Admin password", default="")
	command_setup.add_argument("--letsencrypt-email", help="Let's Encrypt email", default="")
	command_setup.add_argument("--db-password", help="Root database (in container) password", default="")
	command_setup.add_argument("--docker-image", help="Docker image", default="")
	command_setup.add_argument("--certificate", help="Use an HTTPS certificate? (yes/no/proxy)", default="")
	command_setup.add_argument("--traefik-domain", "--traefik-dashboard", help="Domain name of the hosted Traefik Dashboard", default="", dest="traefik_domain")
	command_setup.add_argument("--traefik-password", help="Password to access the Traefik Dashboard", default="")
	command_setup.add_argument("--default-apps", help="List of apps to install by default (comma separated without whitespace)", default="")
	command_setup.add_argument("--wildcard-domain", help="Wildcard domain for SSL (`-' to disable)", default="")
	command_setup.add_argument("--sites", help="List of sites to create (comma separated without whitespace)", default="")

	command_up = commands.add_parser("up", help="Start environment")
	command_down = commands.add_parser("down", help="Stop environment")
	command_pull = commands.add_parser("pull", help="Pull latest images")
	command_status = commands.add_parser("status", help="Show status of Docker projects")
	command_restart = commands.add_parser("restart", help="Restart all containers")
	command_destroy = commands.add_parser("DESTROY", help="Destroy environment")
	command_autoupdate = commands.add_parser("install-autoupdate", help="Install autoupdate timer")

	command_upgrade = commands.add_parser("upgrade", help="Pull containers and migrate all sites of the environment")
	command_upgrade.add_argument("--image", help="Skip backup", dest="to_image", default="")
	command_upgrade.add_argument("--tag", help="Skip backup", dest="to_tag", default="")
	command_upgrade.add_argument("--force", help="Force upgrade even if no new version is available", action="store_true")

	command_upgrade_manage = commands.add_parser("upgrade-manage", help="Upgrade this script")
	command_upgrade_everything = commands.add_parser("upgrade-everything", help="Upgrade this script and all containers")

	# command_revert = commands.add_parser("REVERT", help="Reverts to the previous state of the environment by restoring")
	command_shell = commands.add_parser("shell", help="Start shell in container")

	command_set_admin_password = commands.add_parser("set-admin-password", help="Set administrator password on all sites")
	command_set_admin_password.add_argument("password", help="New password")

	command_docker = commands.add_parser("docker", help="Run docker command in container")
	command_docker.add_argument("docker_command", nargs=argparse.REMAINDER, help=argparse.SUPPRESS)

	# Read all arguments from the command line, even flags
	command_bench = commands.add_parser("bench", help="Run bench command container")
	command_bench.add_argument("bench_args", nargs=argparse.REMAINDER, help=argparse.SUPPRESS)

	command_site = commands.add_parser("site", help="Site management")
	site_commands = command_site.add_subparsers(dest="site_command", help="Site command to run")

	command_site_create = site_commands.add_parser("create", help="Create site")
	command_site_create.add_argument("site", help="Site name")
	command_site_create.add_argument("--apps", help="List of apps to install (comma separated without whitespace)", default="")

	command_site_list = site_commands.add_parser("list", help="List sites")

	command_site_update = site_commands.add_parser("fix-list", help="Fix site list")

	command_site_delete = site_commands.add_parser("DROP", help="Delete site")
	command_site_delete.add_argument("site", help="Site name")

	command_site_delete = site_commands.add_parser("rename", help="Rename site")
	command_site_delete.add_argument("site", help="Site name")
	command_site_delete.add_argument("new_name", help="New site name")

	command_site_backup = site_commands.add_parser("backup", help="Backup site")
	command_site_backup.add_argument("site", help="Site name")

	command_site_console = site_commands.add_parser("console", help="Open site console")
	command_site_console.add_argument("site", help="Site name")

	command_site_restore = site_commands.add_parser("restore-zip", help="Restore site from external ZIP archive.")
	command_site_restore.add_argument("site", help="Site name")
	command_site_restore.add_argument("--zip-file", help="Path to the ZIP archive", default="local")
	command_site_restore.add_argument("--zip-password", help="Password to decrypt the ZIP archive", default="")
	command_site_restore.add_argument("--zip-cookies", help="Cookies to use to download the ZIP archive", default="")
	command_site_restore.add_argument("--exclude-apps", help="Exclude some apps before migrate", default="")

	# command_site_restore = site_commands.add_parser("restore", help="Restore site from project/backups/... backups.")
	# command_site_restore.add_argument("site", help="Site name")
	# command_site_restore.add_argument("backup-dir", help="Path to the ZIP archive")

	command_site_update = site_commands.add_parser("update", help="Update site")
	command_site_update.add_argument("site", help="Site name")

	command_site_migrate = site_commands.add_parser("migrate", help="Migrate site")
	command_site_migrate.add_argument("site", help="Site name")

	command_site_install_app = site_commands.add_parser("install-app", help="Install app on site")
	command_site_install_app.add_argument("site", help="Site name")
	command_site_install_app.add_argument("app", help="App name")

	argv = list(sys.argv[1:])

	if argv and argv[0] in ("bench", "docker"):
		# Hack to allow passing flags to bench
		argv.insert(1, "--")

	args = parser.parse_args(argv)

	if args.command == "install":
		Dependencies().install_all()
		sys.exit(0)
	elif args.command == "upgrade-manage":
		Dependencies().upgrade_self()
		sys.exit(0)

	manager = Manager(args.project)
	# manager.refresh()

	if args.command == "setup":
		manager.cmd_setup(args)
		sys.exit(0)
	elif manager.needs_setup():
		raise RuntimeError(f"Setup is not done, please run `{sys.argv[0]} setup` first.")

	start_time = time.monotonic()

	try:
		match args.command:
			case "shell":
				manager.cmd_shell()
			case "docker":
				if "--" in args.docker_command:
					args.docker_command.remove("--")
				manager.cmd_docker(args.docker_command)
			case "bench":
				if "--" in args.bench_args:
					args.bench_args.remove("--")
				manager.cmd_bench(args.bench_args)
			case "set-admin-password":
				manager.cmd_set_admin_password(args.password)
			case "up":
				manager.cmd_up()
			case "down":
				manager.cmd_down()
			case "pull":
				manager.cmd_pull()
			case "status":
				manager.cmd_status()
			case "restart":
				manager.cmd_restart()
			case "DESTROY":
				manager.cmd_destroy()
			case "upgrade":
				manager.cmd_upgrade(args.to_tag, args.to_image, args.force)
			case "upgrade-everything":
				try:
					upgraded1 = Dependencies().upgrade_self()
					upgraded2 = manager.cmd_upgrade()
					if upgraded1 or upgraded2:
						manager.cmd_restart()
						manager.log_remote("Updated successfully")
				except Exception as e:
					manager.log_remote("Upgrade failed", e)
					manager.cmd_up()
			case "install-autoupdate":
				manager.cmd_setup_autoupdate()
			# case "REVERT":
			# 	manager.cmd_revert()
			case "site":
				match args.site_command:
					case "restore-zip":
						manager.cmd_site_restore(
							site=args.site,
							zip_file=args.zip_file,
							zip_password=args.zip_password,
							zip_cookies=args.zip_cookies,
							exclude_apps=args.exclude_apps,
						)
					case "create":
						manager.cmd_site_new(args.site, args.apps)
					case "list":
						manager.cmd_site_list()
					case "fix-list":
						manager.cmd_fix_site_list()
					case "DROP":
						manager.cmd_site_drop(args.site)
					case "rename":
						manager.cmd_site_rename(args.site, args.new_name)
					case "console":
						manager.cmd_site_console(args.site)
					case "backup":
						manager.cmd_site_backup(args.site)
					case "install-app":
						manager.cmd_install_app(args.site, args.app)
					case "migrate":
						manager.cmd_site_migrate(args.site)
					case _:
						command_site.print_help()
						sys.exit(1)
			case _:
				parser.print_help()
				sys.exit(1)
	except CommandFailed:
		sys.exit(1)

	end_time = time.monotonic()
	cprint(f"{end_time - start_time:.2f} seconds", level=2, dim=1)
